<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jugadores extends Model
{
    use HasFactory;

    //Hacemos referencia a la tabla que vamos a registrar
    protected $table = 'jugadores';

    //Hacemos referencia a los campos que vamos a usar 
    protected $fillable = 
    [
        'nombreJugador',
        'idJugador',
        'idPartida',
        'Turno'
    ];

    //Hacemos referencia a los campos que vamos a ocultar
    protected $hidden =
    [
        'created_at',
        'updated_at'
    ];
}
