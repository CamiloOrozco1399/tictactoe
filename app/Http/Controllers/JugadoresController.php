<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\jugadores;

class JugadoresController extends Controller
{
    //Redirecciona al inicio del juego
    public function index()
    {
        return view('Game.index');
    }

    //Redirecciona al inicio del juego
    public function create()
    {
        return view('Game.index');
    }

    //Se realiza su respectiva insercion a nuevo juego
    public function store(Request $request)
    {

        $length = 6;
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';

    for ($i = 0; $i < $length; $i++) 
    {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

        $jugador = new jugadores;
        $jugador->nombreJugador = $request->input('nombreJugador');
        $jugador->idJugador = 1;
        $jugador->idPartida = $randomString;
        $jugador->Turno = 2;
        $jugador->save();
        return view('Game.create', compact('jugador'));
    }

    //Se realiza la consulta para unirse a la partida y se envian los datos a la vista 
    public function show(Request $request)
    {
        $idPartida = $request->input('partida');
        $jugador = DB::table('jugadores')->where('idPartida', '=',$idPartida )->first();
        return view('Game.Create', compact('jugador'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

   
}
