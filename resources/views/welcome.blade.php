@extends('layouts.app')
@section('content')

<div>

    <div class="botones">
        <div class="juego">
            <a type="button" class="btn btn-success btn-lg bton" >Nueva partida</a>
            <button type="button" class="btn btn-primary btn-lg bton" data-bs-toggle="modal" data-bs-target="#exampleModal">Ingresar a una partida</button>
        </div>    
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLabel">Unirse a una partida</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <input type="text" class="form-control" placeholder="Ingrese el ID de la partida">
        </div>
        <div class="modal-footer text-center">
            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-success">Unirse</button>
        </div>
        </div>
    </div>
    </div>

</div>
@endsection