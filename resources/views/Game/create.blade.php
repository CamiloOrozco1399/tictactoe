@extends('layouts.app')
@section('content')

    <!--Se valida el objeto y se asigan a cada campo-->
    @if ($jugador )
    <div class="partida">
        <div class="regresar">
            <a class="btn btn-primary btn-lg " href="{{ url('http://localhost:8000/') }}">Regresar</a>
        </div>
        <h4> ID Partida {{$jugador->idPartida}} </h4>
    </div>
    
        <div class="centro">
                <div class="jugadores">{{ $jugador->nombreJugador }}</div>
                <div class="jugadores">VS</div>
                <div class="jugadores">Jugador 2</div>
        </div>
    

    <form action="{{ route('jugadores.show')}}" method="get">
        <input type="hidden" name="partida" value="{{$jugador->idPartida}}">
        <div class="botones">
            <div class="juego">

                @php
                    $error = false;
                    $xwins = false;
                    $owins = false;
                    $count = 0;
                @endphp
                <!--Se crea un for el cual va a crear los campos-->
                @for ($id=1;$id<=9;$id++)
                    
                    @if ($id == 4 or $id==7 )<br>@endif
                        <input type="text" name="campo{{ $id }}">
                    @if ('submit' == "" and $id == "" )
                        @if ($id == "x" or $id == "o")
                            @php
                                $count +=1;
                                echo "<input name = $id type = text"
                            @endphp

                            <!--Se realiza un recorrido validando las casillas marcadas para ganar -->
                            @for ($a = 1, $b = 2, $c = 3; $a<= 7, $b<=8, $c <= 9; $a+=3, $b+=3, $c+=3)
                                @if ($datos[0] == $datos[1] and $datos[1] == $datos[2])
                                    @if ($datos[0] == "x")
                                        @php
                                            $xwins = true;
                                        @endphp
                                    @endif
                                    @elseif ($datos[0] == "o")
                                        @php
                                            $owins = true;
                                        @endphp
                                    
                                @endif
                            @endfor

                            <!--Se realiza un recorrido validando las casillas marcadas para ganar--> 
                            @for ($a = 1, $b = 4, $c = 7; $a<= 3, $b<=6, $c <= 9; $a+=1, $b+=1, $c+=1)
                                @if ($a == $b and $b == $c)
                                    @if ($a == "x")
                                        @php
                                            $xwins = true;
                                        @endphp
                                    @endif
                                    @elseif ($a == "o")
                                        @php
                                            $owins = true;
                                        @endphp
                                    
                                @endif
                            @endfor

                            <!--Se realiza un recorrido validando las casillas marcadas para ganar -->
                            @for ($a = 1, $b = 5, $c = 9; $a<= 3, $b<=5 , $c <= 9; $a+=2, $b+=5, $c-=2)
                            @if ($a == $b and $b == $c)
                                @if ($a == "x")
                                    @php
                                        $xwins = true;
                                    @endphp
                                @endif
                                @elseif ($a == "o")
                                    @php
                                        $owins = true;
                                    @endphp
                                
                            @endif
                        @endfor

                        @else
                            @php
                                $error = true;
                            @endphp
                        @endif
                    @endif
                @endfor
                <button class="btn btn-success" name="submit" type="submit">Enviar</button>
            </div>
        </div>
    </form>
    <!--Se realiza su respectiva validacion, asignando los mensajes correspondistes-->
    @if ($xwins)
        <h1>Gano x</h1>
    @else
        <h3>Gano o</h3>
    @endif
    @if ($count == 9 and $xwins == false and $owins == false)
        <h3>Empate</h3>
    @else
        <h3>Inserte un valor entre "x" o "o"</h3>
    @endif

    @endif
@endsection