@extends('layouts.app')
@section('content')

<div>
    <!--Se crear los botones de nueva partida e ingresar a una partida-->
    <div class="botones">
        <div class="juego">
            <button type="button" class="btn btn-success btn-lg bton" data-bs-toggle="modal" data-bs-target="#crear">Nueva partida</button>
            <button type="button" class="btn btn-primary btn-lg bton" data-bs-toggle="modal" data-bs-target="#entrar">Ingresar a una partida</button>
        </div>    
    </div>

    <!--Se crear un modal el cual nos registrara el nombre del jugador y nos da inicio al juego-->
    <form action="{{ route('jugadores.store')}}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="modal fade" id="crear" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="exampleModalLabel">Escriba el nombre de usuario</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                        <div class="modal-body">
                            <input type="text" class="form-control" value="Jugador 1" name="nombreJugador">
                        </div>
                        <div class="modal-footer text-center">
                            <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-success">Unirse</button>
                        </div>
                </div>
            </div>
        </div>
    </form>

    <!--Se crea un modal el cual nos consultara si el codigo de la partida es correcto e jugaremos -->
    <form action="{{ route('jugadores.show')}}" method="get" enctype="multipart/form-data">
    @csrf
        <div class="modal fade" id="entrar" tabindex="-1" aria-labelledby="exampleModalLabel1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="exampleModalLabel">Unirse a una partida</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <input type="number" class="form-control" name="partida" placeholder="Ingrese el ID de la partida">
                    </div>
                    <div class="modal-footer text-center">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-success">Unirse</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

</div>
@endsection