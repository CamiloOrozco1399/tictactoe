<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    
    //Anexamos nuestra hoja de seeder para poder registrarla 
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([JugadoresSeeder::class]);
    }
}
