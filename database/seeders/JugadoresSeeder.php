<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JugadoresSeeder extends Seeder
{
    
    //Creamos semillas para hacer pruebas en la base de datos
    public function run()
    {
        DB::table('jugadores')->insert([
            [
                'nombreJugador' => 'camilo',
                'idJugador' => '1',
                'idPartida' => '123456',
                'Turno' => '1'
            ],
            [
                'nombreJugador' => 'carlos',
                'idJugador' => '2',
                'idPartida' => '654321',
                'Turno' => '2'
            ]
        ]);
    }
}
