<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JugadoresController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Game.index');
});

//Se crear las rutas para la comunicacion con el formulario
Route::get('/jugadores', [JugadoresController::class,'create'])->name('Game/create');
Route::post('/jugadores', [JugadoresController::class,'store'])->name('jugadores.store');
Route::get('/jugadores', [JugadoresController::class,'show'])->name('jugadores.show');